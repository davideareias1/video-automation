import sys
import os
import moviepy.editor as mp

# Function to speed up a video to match a target length
def speed_up_video(input_path, book_name):
    # Extract the filename from the input path
    filename = os.path.splitext(os.path.basename(input_path))[0]

    video_clip = mp.VideoFileClip(input_path)

    folder_path = "output/"+book_name
    os.makedirs(folder_path) if not os.path.exists(folder_path) else None

    output_path = "output/"+book_name+"/"+filename+".mp4"
    # Check if the video is already 60 seconds or shorter
    if video_clip.duration <= 59:
        print(f"Skipping {input_path} - Already 60 seconds or shorter.")
        video_clip.write_videofile(output_path, codec='libx264', audio_codec='aac')
        return

    # Calculate the speedup factor to make the video 60 seconds long
    speedup_factor = 59.0 / video_clip.duration
    sped_up_clip = video_clip.fx(mp.vfx.speedx, speedup_factor)
    final_clip = sped_up_clip.subclip(0, 59)

    # Save the video with audio in MP4 format
    final_clip.write_videofile(output_path, codec='libx264', audio_codec='aac')
    
    # Print a message when the video is saved
    print(f"Video saved as {output_path}")

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("Usage: python script.py input_video.mp4 book_name")
    else:
        input_video = sys.argv[1]
        book_name = sys.argv[2]
        speed_up_video(input_video, book_name)
