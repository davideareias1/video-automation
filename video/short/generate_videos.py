import os
import subprocess
import re
import argparse
from capcut_video_edit import EditDownload
from selenium.common.exceptions import ElementNotInteractableException

def process_files(book_name, aspect_ratio, speaker):
    book_folder = "book/books/" + book_name

    # Define the maximum number of retries
    max_retries = 3

    # Iterate through files in the book folder
    for filename in os.listdir(book_folder):
        print(filename)
        script = ""
        with open(os.path.join(book_folder, filename), "r", encoding="utf-8") as file:
            script = file.read()

        retry_count = 0
        success = False

        while retry_count < max_retries:
            try:
                # Call request.py with the provided script, aspect_ratio, and speaker
                command = ["python", "short/capcut_video_generator.py", "--script", script, "--aspect_ratio", aspect_ratio, "--speaker", speaker, "--filename", filename[:-4], "--name_of_book", book_name]
                output = subprocess.check_output(command)

                print(output)
                pattern = r'Done!\r\n([A-Z0-9-]+)\r\n'
                # Use re.search to find the pattern in the input string
                match = re.search(pattern, output.decode())

                if match:
                    draft_key = match.group(1)
                    success = True
                    break
                else:
                    print("Code not found in the draft_key. Retrying...")
                    retry_count += 1
            except (subprocess.CalledProcessError, subprocess.TimeoutExpired, ElementNotInteractableException) as e:
                print(f"Failed to process {filename}. Error: {e}. Retrying...")
                retry_count += 1

        if not success:
            print(f"Failed to process {filename} after {max_retries} retries. Skipping to the next file.")
            continue

        # Edit video
        url = "https://www.capcut.com/editor/"+draft_key+"?from_page=smart_tool_page&__tool_type=text_to_video&workspaceId=7291615550567120897&spaceId=7291614237846832130&draftIdc=1&__tool_position=edit_more&enter_from=draft"
        download_window = EditDownload()
        download_window.setup_method()
        download_window.edit_download(url, filename[:-4])
        download_window.teardown_method()

        # Add music
        command = ["python", "short/add_music.py", "short/temp/"+filename[:-4]+".mp4", "short/music/classic.mp3"]
        subprocess.run(command)

        # Make it 60 seconds
        command = ["python", "short/speed_up.py", "short/temp/"+filename[:-4]+".mp4", book_name]
        subprocess.run(command)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process book files.")
    parser.add_argument("book_name", help="Name of the book")
    parser.add_argument("aspect_ratio", help="Aspect ratio")
    parser.add_argument("speaker", help="Speaker")
    args = parser.parse_args()

    process_files(args.book_name, args.aspect_ratio, args.speaker)