import os
from chatgpt_selenium_automation import ChatGPTAutomation
import emoji
import re
import sys


def init():
    # Define the path where the chrome driver is installed on your computer
    chrome_driver_path = r"chromedriver.exe"

    # The syntax r'"..."' is required because of the space in "Program Files" in my chrome_path
    chrome_path = r'"C:\Program Files\Google\Chrome\Application\chrome.exe"'

    # Create an instance
    return ChatGPTAutomation(chrome_path, chrome_driver_path)
    

def get_chapters(book_name, chatgpt):
    # Construct the prompt to query ChatGPT
    prompt = "Please analyze the book "+book_name+" and provide a comprehensive list of its most significant chapters. Include all relevant chapters without any limitations. Very important to ensure that you only provide the content without any introductions or additional commentary or explanations about what you wrote I dont want the author of the book just the chapters raw"
    chatgpt.send_prompt_to_chatgpt(prompt)

    # Retrieve the response from ChatGPT
    response = chatgpt.return_last_response()

    # Return the response
    return response


def clean_text(text):
    text_without_emojis = ''.join(c for c in text if c not in emoji.EMOJI_DATA)
    text_without_hashtags = ' '.join(word for word in text_without_emojis.split() if not word.startswith("#"))
    cleaned_response = re.sub(r'\[.*?\]', '', text_without_hashtags)
    cleaned_response = cleaned_response.replace('"', '')
    return cleaned_response

def gen_chapter(chapter, book_name, chatgpt):
    prompt = 'You are now a serius motivational person doing a youtube short about 60 seconds. You will write the text about: ' + chapter + 'from the book: ' + book_name + '. The text is eyecathing. The text has initial hook to grap views attention, something starting like "Imagine", "This"  The text has an example in order to make the viewer see himself in the context The text ends with a motivational ending. Dont use: ":" [Opening with a hook to grab viewers attention] Dont use Emojis or hastags'
    chatgpt.send_prompt_to_chatgpt(prompt)
    response = chatgpt.return_last_response()

    with open("book/books/"+book_name+"/"+chapter+".txt", "w",encoding="utf-8") as file:
        cleaned_response = clean_text(response)
        print("generated video "+ chapter)
        file.write(cleaned_response)
    

def gen_chapters(book_name, chapters, chatgpt, ignore_lines):
    os.makedirs("book/books/"+book_name, exist_ok=True)


    chapters = chapters.split('\n')
    for chapter in chapters[ignore_lines:]:
        chapter = re.sub(r'[^a-zA-Z ]', '', chapter)
        gen_chapter(chapter, book_name, chatgpt)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python your_script.py <book_name>")
    else:
        book_name = sys.argv[1]
        chatgpt = init()
        chapters = get_chapters(book_name, chatgpt)
        print(chapters)
        ignore_lines = input("Number of lines to ignore: ")
        gen_chapters(book_name, chapters, chatgpt, int(ignore_lines))
        chatgpt.quit()
