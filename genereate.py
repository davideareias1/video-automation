import subprocess

def run_command(command):
    try:
        subprocess.run(command, shell=True, check=True)
    except subprocess.CalledProcessError as e:
        print(f"Command failed with exit code {e.returncode}: {e.cmd}")
        exit(1)

def main():
    book_name = input("Enter the book name: ")
    aspect_ratio = "9:16"  # Default aspect ratio
    speaker = "en_male_narration"  # Default speaker

    # Prompt for aspect ratio and speaker, if provided
    aspect_ratio_input = input("Enter the aspect ratio (default is 9:16): ")
    if aspect_ratio_input:
        aspect_ratio = aspect_ratio_input

    speaker_input = input("Enter the speaker (default is en_male_narration): ")
    if speaker_input:
        speaker = speaker_input

    # Display a summary of the options
    print("Options:")
    print(f"Book Name: {book_name}")
    print(f"Aspect Ratio: {aspect_ratio}")
    print(f"Speaker: {speaker}")

 # Prompt for confirmation
    confirm = input("Run? (y or n): ")
    if confirm.lower() == "n":
        print("Script execution canceled.")
        exit(0)


    command = ["python", "book/generate_book.py", book_name]
    #subprocess.run(command)

    command = ["python", "short/generate_videos.py", book_name, aspect_ratio, speaker]
    #subprocess.run(command)

    command = ["python", "youtube/youtube_upload.py", book_name]
    subprocess.run(command)

if __name__ == "__main__":
    main()
