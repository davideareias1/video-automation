import os
import requests
import time
import shutil
import argparse
from urllib.parse import urljoin

# Define common headers
COMMON_HEADERS = {
    "authority": "edit-api-sg.capcut.com",
    "method": "POST",
    "scheme": "https",
    "Accept": "application/json, text/plain, */*",
    "Accept-Encoding": "gzip, deflate, br",
    "Accept-Language": "pt-PT,pt;q=0.9,pt-BR;q=0.8,en;q=0.7,en-US;q=0.6,en-GB;q=0.5",
    "App-Sdk-Version": "48.0.0",
    "Appvr": "5.8.0",
    "Content-Type": "application/json",
    "Cookie": "",
    "Lan": "en",
    "Loc": "va",
    "Origin": "https://www.capcut.com",
    "Pf": "7",
    "Referer": "https://www.capcut.com/",
    "Sec-Ch-Ua": "\"Chromium\";v=\"118\", \"Microsoft Edge\";v=\"118\", \"Not=A?Brand\";v=\"99\"",
    "Sec-Ch-Ua-Mobile": "?0",
    "Sec-Ch-Ua-Platform": "\"Windows\"",
    "Sec-Fetch-Dest": "empty",
    "Sec-Fetch-Mode": "cors",
    "Sec-Fetch-Site": "same-site",
    "Sign-Ver": "1",
    "Tdid": "",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36"
}

def login():
    # Define the URL for the login request
    url = "https://www.capcut.com/passport/web/email/login/?aid=348188&account_sdk_source=web&sdk_version=2.1.2-abroad-beta.0&language=en&verifyFp=verify_loa738iz_2XNx1Vct_GB6x_4ipv_B69m_9mZahxlkM4Pr"

    # Define the data to be sent in the request
    data = {
        "aid": "348188",
        "account_sdk_source": "web",
        "sdk_version": "2.1.2-abroad-beta.0",
        "language": "en",
        "verifyFp": "verify_loa738iz_2XNx1Vct_GB6x_4ipv_B69m_9mZahxlkM4Pr",
        "mix_mode": "1",
        "email": "6164736c61606477606c647634456268646c692b666a68",  # You might want to decode this from hex
        "password": "4164736c6160313d333024",  # You might want to decode this from hex
        "fixed_mix_mode": "1"
    }

    # Define the headers to be sent in the request
    headers = {
        "Accept": "application/json, text/javascript",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "pt-PT,pt;q=0.9,pt-BR;q=0.8,en;q=0.7,en-US;q=0.6,en-GB;q=0.5",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36",
        # Add other headers here as needed
    }

    # Make the POST request
    response = requests.post(url, data=data, headers=headers)

    cookies = {}
    set_cookie_headers = response.headers.get('Set-Cookie', '').split(',')
    for header in set_cookie_headers:
        cookie_parts = header.strip().split(';')
        cookie_name, cookie_value = cookie_parts[0].split('=', 1)
        cookies[cookie_name] = cookie_value

    # Convert the cookies dictionary to a string
    cookie_string = "; ".join([f"{key}={value}" for key, value in cookies.items()])

    return cookie_string

def generate_headers(path, content_length, device_time, sign):
    specific_headers = {
        "path": path,
        "Content-Length": content_length,
        "Device-Time": device_time,
        "Sign": sign,
    }

    headers = {**COMMON_HEADERS, **specific_headers}
    return headers

def send_post_request(url, headers, data):
    response = requests.post(url, headers=headers, json=data)
    return response

def send_get_request(url, headers):
    response = requests.get(url, headers=headers)
    return response

def extract_video_urls(m3u8_url):
    response = requests.get(m3u8_url)

    if response.status_code == 200:
        playlist_lines = response.text.split('\n')
        video_urls = []
        base_url = m3u8_url.rsplit('/', 1)[0]

        for line in playlist_lines:
            if line.startswith("https://"):
                video_urls.append(line)
            elif line and not line.startswith("#"):
                # If the line is not empty and not a comment, construct the full URL
                full_url = urljoin(base_url, line)
                video_urls.append(full_url)

        return video_urls
    return []

def download_and_concatenate_videos(video_urls, output_filename, name_of_book):
    # Create the directory if it doesn't exist
    directory = "temp/"+name_of_book+"/"
    os.makedirs(directory, exist_ok=True)

    video_files = []
    for i, video_url in enumerate(video_urls):
        video_filename = os.path.join(directory, f"{output_filename}_{i}.ts")
        with requests.get(video_url, stream=True) as r:
            if r.status_code == 200:
                with open(video_filename, 'wb') as f:
                    shutil.copyfileobj(r.raw, f)
                video_files.append(video_filename)
                print(f"Downloaded segment {i}")
            else:
                print(f"Failed to download segment {i} with status code {r.status_code}")

    # Concatenate video segments
    concatenated_filename = os.path.join(directory, f"{output_filename}.ts")
    with open(concatenated_filename, 'wb') as output_file:
        for video_file in video_files:
            with open(video_file, 'rb') as input_file:
                shutil.copyfileobj(input_file, output_file)

    # Delete the individual video segments
    for video_file in video_files:
        os.remove(video_file)

    print(f"Concatenated video saved as {concatenated_filename}")

def render(task_id, package_id, filename, name_of_book):
    # Define the URL for the third request
    render_create_url = "https://edit-api-sg.capcut.com/lv/v1/intelligence/render_create"

    # Define the request data for the third request using the task_id and the new package_id
    render_create_data = {
        "workspace_id": "7291615550567120897",
        "task_id": task_id,  # Use the task_id from the second response
        "package_id": package_id,  # Use the new package_id
    }

    render_create_headers = generate_headers("/lv/v1/intelligence/render_create", "105", "1698498723", "9a85ed20af44668694c217e1cb633fc0")

    # Send the third POST request
    response = send_post_request(render_create_url, render_create_headers, render_create_data)

    # Check the response of the third request
    if response.status_code == 200:
        # Request was successful
        response_data = response.json()
        play_url = response_data.get("data", {}).get("play_url")

        if play_url:
            # Extract the video file URL from the M3U8 playlist
            video_url = extract_video_urls(play_url)

            if video_url:
                # Download the video file using requests
                download_and_concatenate_videos(video_url, filename, name_of_book)
            else:
                print("Video URL not found in the M3U8 playlist")
        else:
            print("play_url not found in the response data")
    else:
        print(f"Third request failed with status code {response.status_code}")

def create_video_task(script, aspect_ratio, speaker):
    create_url = "https://edit-api-sg.capcut.com/lv/v1/intelligence/create"

    # Define the request data for the first request
    create_data = {
        "workspace_id": "7291615550567120897",
        "smart_tool_type": 15,
        "params": '{"text":"' + script + '","mode":0,"speech_reader":"' + speaker + '","tone_effect_id":"3438627","aspect_ratio":"' + aspect_ratio + '","event_info":"{}"}'
    }

    headers = generate_headers("/lv/v1/intelligence/create", "233", "1698498715", "f5ba7245accda0ed7a556ee28831a93f")

    # Send the first POST request with the second header
    response = send_post_request(create_url, headers, create_data)

    return response

def query_create_video_task(filename, name_of_book, response):
    # Request was successful
    response_data = response.json()

    print(response_data)

    if response_data.get('errmsg') != 'success':
        exit()

    # Extract the task_id from the first response
    task_id = response_data.get("data", {}).get("task_id")

    # Define the URL for the second request
    query_url = "https://edit-api-sg.capcut.com/lv/v1/intelligence/query"

    counter = 0
    while True:
        query_headers = generate_headers("/lv/v1/intelligence/query", "91", "1698498716", "b7068ca024d635a4f924f0a710199380")

        # Define the request data for the second request using the obtained task_id
        query_data = {
            "workspace_id": "7291615550567120897",
            "smart_tool_type": 15,
            "task_id": task_id
        }
        # Send the second POST request
        response = send_post_request(query_url, query_headers, query_data)

        # Check the response of the second request
        if response.status_code == 200:
            # Request was successful
            response_data = response.json()
            status = response_data.get("data", {}).get("status")

            if status == 2:
                print("Done!")
                package_id = response_data.get("data", {}).get("draft_info", {}).get("package_id")
                draft_key = response_data.get("data", {}).get("draft_info", {}).get("draft_key")

                #render(task_id, package_id, filename, name_of_book)
                print(draft_key) 
                break  # Exit the loop if the server is ready
            elif status == 1:
                print("Generating...")
                counter += 1
                if counter >= 60:
                    return
            else:
                print("Unknown server status")
        else:
            print(f"Second request failed with status code {response.status_code}")

        # Sleep for one second before making the next request
        time.sleep(1)

def main(script, aspect_ratio, speaker, filename, name_of_book):
    cookies = login()
    COMMON_HEADERS["Cookie"] = cookies
    response = create_video_task(script, aspect_ratio, speaker)

    if response.status_code == 200:
        query_create_video_task(filename, name_of_book, response)
    else:
        print(f"First request failed with status code {response.status_code}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process video creation parameters")
    parser.add_argument("--script", type=str, help="The script to be used in the video")
    parser.add_argument("--aspect_ratio", type=str, help="The aspect ratio for the video")
    parser.add_argument("--speaker", type=str, help="The speaker for the narration")
    parser.add_argument("--filename", type=str, help="File name")
    parser.add_argument("--name_of_book", type=str, help="Name of book")

    args = parser.parse_args()

    if not args.script or not args.aspect_ratio or not args.speaker or not args.filename or not args.name_of_book:
        print("Please provide script, aspect_ratio, and speaker arguments.")
    else:
        main(args.script, args.aspect_ratio, args.speaker, args.filename, args.name_of_book)