import argparse
import moviepy.editor as mpe
import os

def add_audio_to_video(video_file, audio_file):
    video_clip = mpe.VideoFileClip(video_file)
    audio_clip = mpe.AudioFileClip(audio_file)
    
    # Make sure the audio duration matches the video duration
    if audio_clip.duration > video_clip.duration:
        audio_clip = audio_clip.subclip(0, video_clip.duration)
    
    # Composite the video and audio
    final_audio = mpe.CompositeAudioClip([video_clip.audio, audio_clip])
    final_clip = video_clip.set_audio(final_audio)
    
    # Set the desired frame rate to 30
    final_clip = final_clip.set_duration(video_clip.duration).set_fps(30)
    
    final_clip.write_videofile(video_file, codec='libx264')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Add additional audio to a video while keeping the original audio.")
    parser.add_argument("video", help="Input video file")
    parser.add_argument("music", help="Additional audio filename")
    args = parser.parse_args()

    # Call the add_audio_to_video function for the specified video file
    add_audio_to_video(args.video, args.music)
