import os
import sys
import google_auth_oauthlib.flow
import googleapiclient.discovery
from googleapiclient.http import MediaFileUpload
from datetime import datetime, timedelta

scopes = ["https://www.googleapis.com/auth/youtube.upload"]

def main(book_name):
    # Disable OAuthlib's HTTPS verification when running locally.
    # *DO NOT* leave this option enabled in production.
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

    api_service_name = "youtube"
    api_version = "v3"
    client_secrets_file = "youtube/youtube_credentials.json"
    folder_path = "output/" + book_name  # Update to the path of the folder containing your videos

    # Load the list of uploaded video titles
    uploaded_video_titles = set()
    with open("youtube/uploaded_videos.txt", "r") as uploaded_file:
        uploaded_video_titles.update(uploaded_file.read().splitlines())

    # Get credentials and create an API client
    flow = google_auth_oauthlib.flow.InstalledAppFlow.from_client_secrets_file(
        client_secrets_file, scopes)
    credentials = flow.run_local_server()
    youtube = googleapiclient.discovery.build(
        api_service_name, api_version, credentials=credentials)

    publish_date = datetime.now() + timedelta(days=1, hours=20)  # Schedule the first video for the next day at 20:00
    upload_count = 0  # Keep track of the number of uploaded videos in this run

    for filename in os.listdir(folder_path):
        if filename.endswith(".mp4") and upload_count < 4:
            video_title = filename[:-4]

            # Check if the video with the same title has been uploaded before
            if video_title in uploaded_video_titles:
                print(f"Video '{video_title}' has already been uploaded. Skipping.")
                continue

            video_path = os.path.join(folder_path, filename)
            publish_date_iso8601 = publish_date.isoformat()  # Convert to ISO 8601 format

            media = MediaFileUpload(video_path, chunksize=1024 * 1024, resumable=True)

            request = youtube.videos().insert(
                part="snippet,status",
                body={
                    "snippet": {
                        "categoryId": "27",
                        "description": "#selfimprovement #reading #readings #motivation",
                        "title": video_title,
                        "publishedAt": publish_date_iso8601  # Use the ISO 8601 string here
                    },
                    "status": {
                        "privacyStatus": "public",
                        "selfDeclaredMadeForKids": False
                    }
                },
                media_body=media
            )

            response = request.execute()
            print(f"Uploaded video '{video_title}' and scheduled for {publish_date_iso8601}")

            # Add the uploaded video title to the list
            uploaded_video_titles.add(video_title)
            upload_count += 1

            publish_date += timedelta(days=1)  # Schedule the next video for the next day at 20:00

    # Update the list of uploaded video titles
    with open("youtube/uploaded_videos.txt", "w") as uploaded_file:
        uploaded_file.write("\n".join(uploaded_video_titles))

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python script.py <book_name>")
        sys.exit(1)
    book_name = sys.argv[1]
    main(book_name)
